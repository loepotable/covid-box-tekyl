#include <SPI.h>
#include <MFRC522.h>
// ecrire ici le numero UID du bon badge
const byte UID_1[4] = {193, 37, 251, 51}; //H
const byte UID_2[4] = {214, 195, 134, 27};//F
//
const int pinLEDVerte = 6; // LED verte
const int pinLEDRouge = 5; // LED rouge

int badge1 = 0;
int badge2 = 0;
int buttonState = 0;
const int pinDecont = 2;
const int pinRST = 9;  // pin RST du module RC522
const int pinSDA = 10; // pin SDA du module RC522
MFRC522 rfid(pinSDA, pinRST);
int contamination = 0;
void setup()
{
  Serial.begin(9600);
  SPI.begin();
  rfid.PCD_Init();
  pinMode(pinLEDVerte, OUTPUT);
  pinMode(pinLEDRouge, OUTPUT);
  pinMode(pinDecont, INPUT);
}
void loop()
{
  //Serial.println(refus);
  buttonState = digitalRead(pinDecont);
  badge1 = 0;
  badge2 = 0;// quand cette variable n'est pas nulle, c'est que le code est refusé
  if (buttonState == HIGH) {
    Serial.println("decontamination");
    contamination = 0;
  }
  if (contamination) {
    digitalWrite(pinLEDRouge, HIGH);
  } else {
    digitalWrite(pinLEDRouge, LOW);
  }
  if (rfid.PICC_IsNewCardPresent())  // on a dédecté un tag
  {
    if (rfid.PICC_ReadCardSerial())  // on a lu avec succès son contenu
    {
      Serial.println("carte lue");
      for (byte i = 0; i < rfid.uid.size; i++) // comparaison avec le bon UID
      {
        if (rfid.uid.uidByte[i] == UID_1[i]) {
          badge1++;
        }
        else if (rfid.uid.uidByte[i] == UID_2[i]) {
          badge2++;
        }
      }
      if (!contamination) {
        digitalWrite(pinLEDRouge, LOW);
        if (badge1 == 4) // UID accepté
        {
          Serial.println("carte acceptée");
          digitalWrite(pinLEDVerte, HIGH);

          contamination = 1;
        }
        else if (badge2 == 4) // UID accepté
        {
          Serial.println("carte acceptée");
          digitalWrite(pinLEDVerte, HIGH);


        }
      }




      /*else if ((badge1 == 0)||(badge2 == 0)) // UID accepté
        {
        Serial.println("carte refusée");
        digitalWrite(pinLEDRouge, HIGH);
        delay(300);
        digitalWrite(pinLEDRouge, LOW);
        }*/

    }
    //Serial.println("ca marche ?");
  } else {
    digitalWrite(pinLEDVerte, LOW);
  }
}
